<?php
require_once "auth.class.php";

abstract class Core {


	protected $pdo;


	public function __constuct() {

		$dsn = "mysql:host=".DB_HOST.";dbname=".DB_NAME.";charset=".CHARSET;
		$opt = array(
   		PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION,
    	PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC
			);
			$pdo = new PDO($dsn, DB_USERNAME, DB_PASSWORD, $opt);
		//$this->pdo = new PDO($dsn, DB_USERNAME, DB_PASSWORD, $opt);


	}

	protected function get_header(){

		include_once THEME."/default/header.php";
	}
	protected function get_content(){

		include_once THEME."/default/index.php";
	}
	protected function get_footer(){

		include_once THEME."/default/footer.php";
	}




	public function get_body() {

		$this->get_header();
		$this->get_content();
		$this->get_footer();


	}

}



 ?>
