<?php

require_once 'config.php';
require_once CLASS_PATH.'core.class.php';
require_once CLASS_PATH.'auth.class.php';


if ($_GET['option']) {
  $class = trim(strip_tags($_GET['option']));
} else {
  $class = 'main';
}

if (file_exists(CLASS_PATH."/".$class.".class.php")) {
  include("classes/".$class.".class.php");

  if (class_exists($class)) {
   $obj = new $class;

   $obj->get_body();


  } else {
    exit("404");
  }

} else {
  exit("404");

}



 ?>
