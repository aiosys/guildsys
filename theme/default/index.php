<?php include_once "header.php"; ?>
<body>
<h1>IMPRESSIVE MULTIPLE FORMS</h1>
	<div class="container">

		<div class="left w3l">
		<h3>Register Form</h3>
			<div class="register agileits">
				<form action="#" method="post">
					<input type="text" class="name" name="username" placeholder="Username" required="">
					<input type="password" class="password" name="password" placeholder="Password" required="">
					<input type="password" class="password" name="confirm password" placeholder="Confirm Password" required="">
					<input type="text" class="email" name="email" placeholder="Email" required="">
					<input type="text" class="location" name="location" placeholder="Your Location" required="">
					<input type="submit" value="Sign Up">
				</form>
			</div>
			<h3>Send Email</h3>
			<div class="send-mail">
				<form action="#" method="post">
					<input type="text" class="name" name="username" placeholder="Your Name" required="">
					<input type="text" class="email" name="email" placeholder="Your Email" required="">
					<textarea name="your message" placeholder="Your message"></textarea>
					<input type="submit" value="Send Email">
				</form>
			</div>
		</div>
		<div class="right">
		<h3>Авторизация</h3>
			<div class="sign-in">
				<?php
if ($auth->isAuth()) { // Если пользователь авторизован, приветствуем:
    echo "Здравствуйте, " . $auth->getLogin() ;
    echo "<br/><br/><a href='?is_exit=1'>Выйти</a>"; //Показываем кнопку выхода
}
else { //Если не авторизован, показываем форму ввода логина и пароля
?>
				<form action="" method="post">
					<input type="text" class="name" name="login" placeholder="Username" required="" value="<?php echo (isset($_POST["login"])) ? $_POST["login"] : null; // Заполняем поле по умолчанию ?>">
					<input type="password" class="password" name="password" placeholder="Password" required="">
					<ul>
						<li>
							<input type="checkbox" id="brand1" value="">
							<label for="brand1"><span></span>Remember me</label>
						</li>
					</ul>
					<input type="submit" value="Войти">
					<div class="clear"></div>
				</form>
				<?php
}
?>
			</div>
			<h3>Recover Form</h3>
			<div class="recover">
				<form action="#" method="post">
					<input type="text" class="email" name="email" placeholder="Email" required="">
					<input type="text" class="number" name="phone number" placeholder="Phone Number" required="">
					<input type="submit" class="send" value="Send">
					<input type="submit" class="done" value="Done">
				</form>
			</div>
			<h3>Subscribe Form</h3>
			<div class="subscribe">
				<form action="#" method="post">
					<input type="text" class="email" name="email" placeholder="Email" required="">
					<input type="submit" value="Subscribe">
					<input type="submit" class="no-thanks" value="No,Thanks">
				</form>
			</div>
			<h3>Follow Us</h3>
			<div class="socialicons w3">
				<ul>
					<li><a class="facebook" href="#"></a></li>
					<li><a class="twitter" href="#"></a></li>
					<li><a class="google" href="#"></a></li>
					<li><a class="pinterest" href="#"></a></li>
					<li><a class="linkedin" href="#"></a></li>
				</ul>
			</div>
		</div>
		<div class="clear"></div>

	</div>
<?php include_once "footer.php"; ?>
</body>
</html>
